![Systemtechnik für Energieeffizienz](img/sye-energylabel.svg)

# Systemtechnik für Energieeffizienz

Hier ist das Repository (Ablage) für das Modul Systemtechnik für Energieeffizienz. Alle dazugehörigen jupter notebooks finden sich hier im Verzeichnis `nb` in der jeweils aktuellsten Version. 

Zum Herunterladen kann man entweder den Download-Knopf verwenden und alle enthaltenen Dateien als zip-Datei herunterladen. Oder man nutzt den Weg, gitlab in einem eigenen Ordner auf dem eigenen Rechner zu synchronisieren. Wenn man Veränderungen in den Dateien vornimmt und diese nicht gelöscht werden sollen, empfiehlt es sich, diese umzubenennen, z. B. von `SYE-beispieldatei.ipynb` in `SYE-beispieldatei-mit-anmerkungen.ipynb`. So behält man die eigenen Notizen, auch, wenn man eines Tages aktuellere Versionen der Dateien hier auf gitlab herunterlädt.

Der beste Startpunkt ist die Datei [SYE.ipynb](SYE.ipynb)

Um die Dateien nutzen zu können, benötigen Sie eine funktionierende python-Installation auf Ihrem Rechner. Beispielsweise können Sie sich [anaconda](https://www.anaconda.com/products/individual) herunterladen. Auf Linux-Rechnern ist in der Regel bereits python vorhanden. Eine zusätzliche python-Installation über anaconda ist daher nicht empfehlenswert. 

Nach der Installation können Sie in anaconda jupyter öffnen und dort zu dem Verzeichnis navigieren, wo Sie die Kursunterlagen entpackt abgespeichert haben. Dann öffnen Sie dort die Datei mit dem Namen [SYE.ipynb](SYE.ipynb) und navigieren von dort aus zu den weiteren Dateien. Unter Linux kann man jupyter notebook im Verzeichnis der Kursunterlagen mit der Konsole starten über den Befehl `jupyter notebook`. 

EDIT!