# ausführen mit:
# $ jupyter nbconvert --to pdf --config config.py

c = get_config()
# c.NbConvertApp.notebooks = ["../SYE.ipynb", "../nb/SYE-batterien.ipynb"] # und so weiter die anderen noch einfügen
c.NbConvertApp.notebooks = ["../SYE.ipynb"]
#c.NbConvertApp.export_format = 'latex'
#c.LatexExporter.exclude_code_cell = False
#c.LatexExporter.exclude_raw = True
c.Exporter.preprocessors = [ 'bibpreprocessor.BibTexPreprocessor', 'pre_pymarkdown.PyMarkdownPreprocessor' ]
c.LatexExporter.template_file = "sye.tplx"
#c.Exporter.preprocessors.append("jupyter_contrib_nbextensions.nbconvert_support.SVG2PDFPreprocessor")
#c.EmbedImagesPreprocessor.embed_images = True
#c.EmbedImagesPreprocessor.embed_remote_images = True

